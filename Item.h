#ifndef ITEM_H
#define ITEM_H

#include <vector>
#include <iostream>

class Item {
	public:
		Item();
		~Item();
		static std::vector<Item> getAll();
		static void addItem(Item);
		static void seed();
		
	protected:
		
	private:
		int qtt;
		std::string name;
		float price;
		static std::vector<Item> itens;
};

#endif